import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/uga')
  sendRateToUGA() {
    return this.appService.sendToGAMPUniversal();
  }

  @Get('ga4')
  sendRateToGA4() {
    return this.appService.sendToGAMP4();
  }
}
