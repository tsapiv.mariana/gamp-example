import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { firstValueFrom } from 'rxjs';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AppService {
  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {}

  gaUA = {
    url: 'https://www.google-analytics.com/collect',
    tid: this.configService.get('TID'),
    cid: this.configService.get('CID'),
    t: 'event',
    ec: 'rate',
    el: 'usd/uah',
  };

  ga4 = {
    url: 'https://www.google-analytics.com/mp/collect',
    api_secret: this.configService.get('API_SECRET'),
    measurement_id: this.configService.get('MEASUREMENT_ID'),
    client_id: this.configService.get('CLIENT_ID'),
  };

  async getUAUSDExchangeRates() {
    const listOfRates = await firstValueFrom(
      this.httpService.get(
        'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json',
      ),
    );
    const uaToUsd = listOfRates.data.find((item) => item['r030'] == 840);
    return uaToUsd;
  }

  async sendToGAMPUniversal() {
    const rateData = await this.getUAUSDExchangeRates();
    const rateUrl = `${this.gaUA.url}?tid=${this.gaUA.tid}&cid=${this.gaUA.cid}&t=${this.gaUA.t}&ec=${this.gaUA.ec}&el=${this.gaUA.el}&value=${rateData.rate}`;
    const res = await firstValueFrom(this.httpService.get(rateUrl));
    return res.status;
  }

  async sendToGAMP4() {
    const rateData = await this.getUAUSDExchangeRates();
    const data = {
      client_id: this.ga4.client_id,
      timestamp_micros: Date.now(),
      events: [
        {
          name: 'currency_rate',
          params: {
            value: rateData.rate,
            currency: `USD/${rateData.cc}`,
            exchangeDate: rateData.exchangedate,
          },
        },
      ],
    };
    const res = await firstValueFrom(this.httpService.post(this.ga4.url, data));
    return res.status;
  }
}
